# C33s Parameter Objects

Inject plain PHP classes which can be autowired into your services instead of binding string parameters.

## Classes and their respective parameters:

- `\C33s\ParameterObjects\KernelEnvironment`: `%kernel.environment%`
- `\C33s\ParameterObjects\KernelProjectDir`: `%kernel.project_dir%`


## Testing

### Environment variables

used by the integration test

- `SYMFONY_VERSION` version constraint which will passed to `composer create-project`
e.g. `~4.4.0`
- `COMPOSER_PHAR` composer phar to use, defaults to the version installed with
robo `.robo/bin/composer.phar`
- `SYMFONY_INTEGRATION_TEST_INITIALIZED` can be set to true to prevent the project
initialization
- `SYMFONY_INTEGRATION_TEST_FORCE_REINITIALIZE` set this to true to force project
reinitialization

also see [adding-new-parameter.md](docs/adding-new-parameter.md)

