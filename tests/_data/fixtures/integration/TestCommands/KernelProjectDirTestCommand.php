<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelProjectDir;

/**
 * Parameter Object Test Command for KernelProjectDir
 */
class KernelProjectDirTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-project-dir';
    private $kernelProjectDir;

    public function __construct(KernelProjectDir $kernelProjectDir)
    {
        $this->kernelProjectDir = $kernelProjectDir;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelProjectDir->value());

        return 0;
    }
}
