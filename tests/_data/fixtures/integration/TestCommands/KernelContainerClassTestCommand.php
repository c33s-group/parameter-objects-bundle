<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelContainerClass;

/**
 * Parameter Object Test Command for KernelContainerClass
 */
class KernelContainerClassTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-container-class';
    private $kernelContainerClass;

    public function __construct(KernelContainerClass $kernelContainerClass)
    {
        $this->kernelContainerClass = $kernelContainerClass;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelContainerClass->value());

        return 0;
    }
}
