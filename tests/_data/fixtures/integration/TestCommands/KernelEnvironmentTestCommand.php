<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelEnvironment;

/**
 * Parameter Object Test Command for KernelEnvironment
 */
class KernelEnvironmentTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-environment';
    private $kernelEnvironment;

    public function __construct(KernelEnvironment $kernelEnvironment)
    {
        $this->kernelEnvironment = $kernelEnvironment;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelEnvironment->value());

        return 0;
    }
}
