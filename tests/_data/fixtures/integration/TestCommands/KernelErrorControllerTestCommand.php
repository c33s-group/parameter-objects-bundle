<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelErrorController;

/**
 * Parameter Object Test Command for KernelErrorController
 */
class KernelErrorControllerTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-error-controller';
    private $kernelErrorController;

    public function __construct(KernelErrorController $kernelErrorController)
    {
        $this->kernelErrorController = $kernelErrorController;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelErrorController->value());

        return 0;
    }
}
