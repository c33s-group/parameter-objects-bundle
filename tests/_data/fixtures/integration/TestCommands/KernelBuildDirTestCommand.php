<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelBuildDir;

/**
 * Parameter Object Test Command for KernelBuildDir
 */
class KernelBuildDirTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-build-dir';
    private $kernelBuildDir;

    public function __construct(KernelBuildDir $KernelBuildDir)
    {
        $this->kernelBuildDir = $KernelBuildDir;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelBuildDir->value());

        return 0;
    }
}
