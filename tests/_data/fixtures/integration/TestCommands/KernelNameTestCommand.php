<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelName;

/**
 * Parameter Object Test Command for KernelName
 */
class KernelNameTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-name';
    private $kernelName;

    public function __construct(KernelName $kernelName)
    {
        $this->kernelName = $kernelName;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelName->value());

        return 0;
    }
}
