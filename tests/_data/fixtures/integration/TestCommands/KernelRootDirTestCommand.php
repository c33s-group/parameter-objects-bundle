<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelRootDir;

/**
 * Parameter Object Test Command for KernelRootDir
 */
class KernelRootDirTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-root-dir';
    private $kernelRootDir;

    public function __construct(KernelRootDir $kernelRootDir)
    {
        $this->kernelRootDir = $kernelRootDir;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelRootDir->value());

        return 0;
    }
}
