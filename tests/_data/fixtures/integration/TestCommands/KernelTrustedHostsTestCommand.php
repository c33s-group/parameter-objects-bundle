<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelTrustedHosts;

/**
 * Parameter Object Test Command for KernelTrustedHosts
 */
class KernelTrustedHostsTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-trusted-hosts';
    private $kernelTrustedHosts;

    public function __construct(KernelTrustedHosts $kernelTrustedHosts)
    {
        $this->kernelTrustedHosts = $kernelTrustedHosts;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelTrustedHosts->value());

        return 0;
    }
}
