<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelLogsDir;

/**
 * Parameter Object Test Command for KernelLogsDir
 */
class KernelLogsDirTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-logs-dir';
    private $kernelLogsDir;

    public function __construct(KernelLogsDir $kernelLogsDir)
    {
        $this->kernelLogsDir = $kernelLogsDir;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelLogsDir->value());

        return 0;
    }
}
