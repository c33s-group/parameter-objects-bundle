<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use C33s\ParameterObjects\KernelDefaultLocale;

/**
 * Parameter Object Test Command for KernelDefaultLocale
 */
class KernelDefaultLocaleTestCommand extends Command
{
    protected static $defaultName = 'test:kernel-default-locale';
    private $kernelDefaultLocale;

    public function __construct(KernelDefaultLocale $kernelDefaultLocale)
    {
        $this->kernelDefaultLocale = $kernelDefaultLocale;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write($this->kernelDefaultLocale->value());

        return 0;
    }
}
