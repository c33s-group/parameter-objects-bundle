<?php

namespace Tests\Integration;

use Tests\IntegrationTester;
use Codeception\Scenario;
use Tests\Integration\Annotations\VersionConstraint;

/**
 * @group integration
 * @VersionConstraint(">=4.4, <5.0")
 */
final class KernelRootDirIntegrationCest
{
    public function testKernelRootDir(IntegrationTester $I, Scenario $scenario)
    {
        $I->skipIfNotInVersionRange($this);
        $I->haveInitializedIntegrationProject();
        $I->assertConsoleCommandOutputEquals('KernelRootDir', 'src/');
    }
}



