<?xml version="1.0"?>
<ruleset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="C33s" namesoace="C33s\PhpCodeSniffer">
    <file>./src</file>
    <file>./RoboFile.php</file>

<!--    for loading via .robo/vendor/squizlabs/php_codesniffer/bin/phpcs https://github.com/squizlabs/PHP_CodeSniffer/issues/2776-->
    <config name="installed_paths" value="../../phpcompatibility/php-compatibility,../../phpcompatibility/phpcompatibility-symfony,../../slevomat/coding-standard" />
<!--    for loading via .robo/bin/phpcs.phar-->
<!--    <config name="installed_paths" value="../vendor/phpcompatibility/php-compatibility,../vendor/phpcompatibility/phpcompatibility-symfony" />-->
    <config name="colors" value="1" />

    <arg name="colors"/>
    <arg value="s"/>

<!--    <exclude-pattern >/src/*</exclude-pattern>-->
    <config name="testVersion" value="7.1-"/>

    <!-- ### Rulesets ############################################################################################## -->
    <rule ref="PSR1"/>
    <rule ref="PSR2"/>
    <rule ref="PHPCompatibility"/>
    <rule ref="PHPCompatibilitySymfonyPolyfillPHP72"/>
    <rule ref="PHPCompatibilitySymfonyPolyfillPHP73"/>
    <rule ref="PHPCompatibilitySymfonyPolyfillPHP74"/>

<!-- ### Rulesets with excludes ################################################################################ -->
    <rule ref="Generic">
        <exclude name="Generic.WhiteSpace.DisallowSpaceIndent.SpacesUsed"/>
        <exclude name="Generic.Files.LowercasedFilename.NotFound"/>
        <exclude name="Generic.PHP.UpperCaseConstant.Found"/>
        <exclude name="Generic.Formatting.SpaceAfterNot.Incorrect"/>
        <exclude name="Generic.Functions.OpeningFunctionBraceKernighanRitchie.BraceOnNewLine"/>
        <exclude name="Generic.Files.EndFileNoNewline.Found"/>
        <exclude name="Generic.PHP.ClosingPHPTag.NotFound"/>
        <exclude name="Generic.Classes.OpeningBraceSameLine.BraceOnNewLine"/>
        <exclude name="Generic.Arrays.DisallowShortArraySyntax.Found"/>
        <exclude name="Generic.Formatting.MultipleStatementAlignment.NotSameWarning"/>
        <exclude name="Generic.Functions.OpeningFunctionBraceBsdAllman.BraceOnSameLine"/>
        <exclude name="Generic.Commenting.DocComment.TagValueIndent"/>
        <exclude name="Generic.Commenting.DocComment.MissingShort"/>
        <exclude name="Generic.Formatting.NoSpaceAfterCast.SpaceFound"/>
        <exclude name="Generic.PHP.RequireStrictTypes.MissingDeclaration"/>
        <exclude name="Generic.ControlStructures.DisallowYodaConditions.Found"/>
        <exclude name="Generic.Commenting.DocComment.ContentAfterOpen"/>
        <exclude name="Generic.Commenting.DocComment.ContentBeforeClose"/>
        <exclude name="Generic.Commenting.DocComment.SpacingBeforeShort"/>
        <exclude name="Generic.Commenting.DocComment.ShortNotCapital"/>
        <!--<exclude name=""/>-->
    </rule>
    <!--<rule ref="PEAR"/>-->
    <!--<rule ref="Squiz"/>-->

    <!-- ### Exact Rules ########################################################################################### -->
<!--    https://github.com/slevomat/coding-standard#slevomatcodingstandardfilestypenamematchesfilename-->
    <rule ref="SlevomatCodingStandard.Files.TypeNameMatchesFileName">
        <properties>
            <property name="rootNamespaces" type="array">
                <element key="src/ParameterObjects" value="C33s\ParameterObjects"/>
                <element key="src/Bundle" value="C33s\Bundle\ParameterObjectsBundle"/>
            </property>
        </properties>
        <exclude-pattern>/Robofile.php</exclude-pattern>
    </rule>
    <rule ref="Squiz.NamingConventions.ValidVariableName.NotCamelCaps"/>
    <rule ref="Squiz.NamingConventions.ValidVariableName.MemberNotCamelCaps"/>
    <rule ref="Squiz.NamingConventions.ValidVariableName.StringNotCamelCaps"/>
    <rule ref="Squiz.NamingConventions.ValidVariableName"/>

    <!-- ### Excluded Rules for Files ############################################################################## -->
<!-- preferably use the config in the file for section and rule specific excludes -->
<!-- https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#ignoring-parts-of-a-file -->

    <rule ref="Generic.Files.LineLength.TooLong">
        <exclude-pattern>/src/SymfonyKernel.php</exclude-pattern>
    </rule>
    <rule ref="PHPCompatibility.IniDirectives.NewIniDirectives.opcache_preloadFound">
        <exclude-pattern>/src/SymfonyKernel.php</exclude-pattern>
    </rule>
    <rule ref="PSR1.Files.SideEffects.FoundWithSymbols">
        <exclude-pattern>/Robofile.php</exclude-pattern>
    </rule>
    <rule ref="PSR1.Classes.ClassDeclaration.MissingNamespace">
        <exclude-pattern>/Robofile.php</exclude-pattern>
    </rule>
    <rule ref="Generic.Files.LineLength.TooLong">
        <exclude-pattern>/Robofile.php</exclude-pattern>
        <!--<exclude-pattern>/public/index.php</exclude-pattern>-->
    </rule>
    <rule ref="Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassBeforeLastUsed">
        <exclude-pattern>/src/Bundle/DependencyInjection/C33sParameterObjectsExtension.php</exclude-pattern>
    </rule>
</ruleset>
