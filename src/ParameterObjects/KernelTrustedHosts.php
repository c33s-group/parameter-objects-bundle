<?php

declare(strict_types=1);

namespace C33s\ParameterObjects;

use C33s\ParameterObjects\Traits\ArrayValueObjectTrait;

final class KernelTrustedHosts
{
    use ArrayValueObjectTrait;
}
