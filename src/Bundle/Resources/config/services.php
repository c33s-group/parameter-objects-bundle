<?php

declare(strict_types=1);

use C33s\ParameterObjects\KernelCacheDir;
use C33s\ParameterObjects\KernelCharset;
use C33s\ParameterObjects\KernelContainerClass;
use C33s\ParameterObjects\KernelDebug;
use C33s\ParameterObjects\KernelDefaultLocale;
use C33s\ParameterObjects\KernelEnvironment;
use C33s\ParameterObjects\KernelErrorController;
use C33s\ParameterObjects\KernelHttpMethodOverride;
use C33s\ParameterObjects\KernelLogsDir;
use C33s\ParameterObjects\KernelName;
use C33s\ParameterObjects\KernelProjectDir;
use C33s\ParameterObjects\KernelRootDir;
use C33s\ParameterObjects\KernelSecret;
use C33s\ParameterObjects\KernelTrustedHosts;
use C33s\ParameterObjects\KernelBuildDir;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Kernel;

/**
 * @codeCoverageIgnore
 */
return static function (ContainerConfigurator $containerConfigurator) {
    $services = $containerConfigurator->services();

    // parameters for symfony lower than 5.0 (tested with 4.4)
    if (Kernel::VERSION_ID < 50000) { /** @phpstan-ignore-line */
        $services->set(KernelName::class)
            ->factory([KernelName::class, 'fromString'])
            ->arg('$value', '%kernel.name%');

        $services->set(KernelRootDir::class)
            ->factory([KernelRootDir::class, 'fromString'])
            ->arg('$value', '%kernel.root_dir%');
    }

    // symfony 5.2 and above
    if (Kernel::VERSION_ID >= 50200) { /** @phpstan-ignore-line */
        $services->set(KernelBuildDir::class)
            ->factory([KernelBuildDir::class, 'fromString'])
            ->arg('$value', '%kernel.build_dir%');
    }

    // generic parameters
    $services->set(KernelCacheDir::class)
        ->factory([KernelCacheDir::class, 'fromString'])
        ->arg('$value', '%kernel.cache_dir%');

    $services->set(KernelCharset::class)
        ->factory([KernelCharset::class, 'fromString'])
        ->arg('$value', '%kernel.charset%');

    $services->set(KernelContainerClass::class)
        ->factory([KernelContainerClass::class, 'fromString'])
        ->arg('$value', '%kernel.container_class%');

    $services->set(KernelDebug::class)
        ->factory([KernelDebug::class, 'fromBool'])
        ->arg('$value', '%kernel.debug%');

    $services->set(KernelDefaultLocale::class)
        ->factory([KernelDefaultLocale::class, 'fromString'])
        ->arg('$value', '%kernel.default_locale%');

    $services->set(KernelEnvironment::class)
        ->factory([KernelEnvironment::class, 'fromString'])
        ->arg('$value', '%kernel.environment%');

    $services->set(KernelErrorController::class)
        ->factory([KernelErrorController::class, 'fromString'])
        ->arg('$value', '%kernel.error_controller%');

    $services->set(KernelHttpMethodOverride::class)
        ->factory([KernelHttpMethodOverride::class, 'fromString'])
        ->arg('$value', '%kernel.http_method_override%');

    $services->set(KernelLogsDir::class)
        ->factory([KernelLogsDir::class, 'fromString'])
        ->arg('$value', '%kernel.logs_dir%');

    $services->set(KernelProjectDir::class)
        ->factory([KernelProjectDir::class, 'fromString'])
        ->arg('$value', '%kernel.project_dir%');

    $services->set(KernelSecret::class)
        ->factory([KernelSecret::class, 'fromString'])
        ->arg('$value', '%kernel.secret%');

    $services->set(KernelTrustedHosts::class)
        ->factory([KernelTrustedHosts::class, 'fromArray'])
        ->arg('$value', '%kernel.trusted_hosts%');
};
